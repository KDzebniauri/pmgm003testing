//importClass(com.sap.gateway.ip.core.customdev.util.Message);
Object.defineProperty(exports, "__esModule", {value: true});
var fs = require("fs");

function removeDuplicates(array, key) {
    var lookup = {};
    var result = [];
    for (var i = 0; i < array.length; i++) {
        if (!lookup[array[i][key]]) {
            lookup[array[i][key]] = true;
            result.push(array[i]);
        }
    }
    return result;
}

var processRawData = function processRawData(pRawData, pFilterYearValue, pFilterPositionValue) {
    /*KET*/// console.log("processRawData >> START");
    var jsonBody = JSON.parse(pRawData);

    if (!(jsonBody && jsonBody.cust_ObjectiveOnPositionDetails && jsonBody.cust_ObjectiveOnPositionDetails.cust_ObjectiveOnPositionDetails)) {
        throw new Error('Empty input content!');
    }

// function processData(message) {
//     var sBody = message.getBody(java.lang.String);
//     var jsonBody = JSON.parse(sBody);
//     if (!(jsonBody && jsonBody.cust_ObjectiveOnPositionDetails && jsonBody.cust_ObjectiveOnPositionDetails.cust_ObjectiveOnPositionDetails)) {
//         throw new Error('Empty input content!');
//     }
//     var filterYearValue = String(message.getProperty("filterYear"));
//     var filterPositionValue = String(message.getProperty("filterPosition"));
//
//     function filterObjectives(objective) {
//
//     return (
//         objective.cust_ObjectiveOnPosition &&
//           (!filterYearValue ||
//             objective.cust_ObjectiveOnPosition.cust_ObjectivePlanNav.PickListValueV2.label_defaultValue &&
//             objective.cust_ObjectiveOnPosition.cust_ObjectivePlanNav.PickListValueV2.label_defaultValue.substr(0, 4) === filterYearValue)
//         );
//     }
    function filterObjectives(objective) {
        return (
            objective.cust_ObjectiveOnPosition &&
            (!pFilterYearValue ||
                objective.cust_ObjectiveOnPosition.cust_ObjectivePlanNav.PickListValueV2.label_defaultValue &&
                objective.cust_ObjectiveOnPosition.cust_ObjectivePlanNav.PickListValueV2.label_defaultValue.substr(0, 4) === pFilterYearValue)
        );
    }

    function pushLines(fromParam, toParam) {
        lines.push({
            from: fromParam,
            to: toParam
        });
    }

    var arrObjectives = jsonBody.cust_ObjectiveOnPositionDetails.cust_ObjectiveOnPositionDetails.filter(filterObjectives);
    var groups = []; // array with duplicate positions
    var nodes = [];
    var lines = [];
    var positionMap = {}; //код цели -> код позиции цели
    var positionParentMap = {}; // код цели -> код родителя позиции на цели
    var positionHierarchyMap = {}; // код позиции -> код ее родителя

    for (var _i = 0; _i < arrObjectives.length; _i++) {
        var oObjective = arrObjectives[_i];
        var oHeader = oObjective.cust_ObjectiveOnPosition;

        var objectivePositionCode = oHeader.cust_ToPositions.Position.code;
        var matrixPositionCode = (oHeader.cust_ToPositions.Position.positionMatrixRelationship.PositionMatrixRelationship && oHeader.cust_ToPositions.Position.positionMatrixRelationship.PositionMatrixRelationship.relatedPositionNav.Position) ? oHeader.cust_ToPositions.Position.positionMatrixRelationship.PositionMatrixRelationship.relatedPositionNav.Position.code : null;
        var parentPositionCode = oHeader.cust_ToPositions.Position.parentPosition.Position ? oHeader.cust_ToPositions.Position.parentPosition.Position.code : null;


        positionMap[oObjective.externalCode] = objectivePositionCode;

        if (matrixPositionCode) {
            positionParentMap[oObjective.externalCode] = matrixPositionCode;
            positionHierarchyMap[objectivePositionCode] = matrixPositionCode;
        } else {
            if (parentPositionCode) {
                positionParentMap[oObjective.externalCode] = parentPositionCode;
                positionHierarchyMap[objectivePositionCode] = parentPositionCode;
            }
        }

        //   positionHierarchyMap[objectivePositionCode]=positionParentMap[oObjective.cust_ObjectiveOnPosition.cust_ToPositions.Position.code]
        //   console.log("efdfd");


    }

    function positionInHierarchyExistenceCheck(positionForCheck, startPosition) {
        var result = false;
        var nextPosition = positionHierarchyMap[startPosition];
        while (!result && nextPosition) {
            if (nextPosition === positionForCheck) {
                result = true;
            } else {
                nextPosition = positionHierarchyMap[nextPosition];
            }
        }
        return result;
    }

    for (var _i = 0; _i < arrObjectives.length; _i++) {
        var oObjective = arrObjectives[_i];
        var oHeader = oObjective.cust_ObjectiveOnPosition;
        var oPosition = oHeader.cust_ToPositions.Position;
        groups.push({
            key: oPosition.code,
            title: oPosition.cust_positionCode + " - " + oPosition.positionTitle + (oPosition.departmentNav.FODepartment ? (" (" + oPosition.departmentNav.FODepartment.name_defaultValue + ")") : "")
        });


        if (oObjective.cust_ParentObjective) {
            if (positionMap[oObjective.externalCode] === positionParentMap[oObjective.cust_ParentObjective]) {
                // Позиция проверяемой цели равна родительской позиции родительской цели
                pushLines(oObjective.externalCode, oObjective.cust_ParentObjective) // стрелка от родительской цели к проверяемой цели
            } else if (positionInHierarchyExistenceCheck(positionMap[oObjective.externalCode], // код позиции исходной цели
                positionMap[oObjective.cust_ParentObjective]))// код позиции родительской цели

            {
                pushLines(oObjective.externalCode, oObjective.cust_ParentObjective) // стрелка от родительской цели к проверяемой цели
            } else {
                // Родительская позиция проверяемой цели равна позиции родительской цели
                pushLines(oObjective.cust_ParentObjective, oObjective.externalCode) // стрелка от проверяемой цели к родительской цели
            }
        }

        // add a line if one exists


        var objectivePlanTypeCode = oHeader.cust_ObjectivePlanTypeNav.PickListValueV2 ? oHeader.cust_ObjectivePlanTypeNav.PickListValueV2.externalCode : null;
        // prepare a node...
        var arrAttributes = [];
        // origin object for node
        var node = {
            key: oObjective.externalCode,
            title: oObjective.cust_GoalName,
            group: oHeader.cust_ToPositions.Position.code,
            shape: 'Box',
            status: objectivePlanTypeCode
        };
        if (objectivePlanTypeCode === '1001') {
            arrAttributes.push({
                'label': 'Единица измерения',
                'value': oObjective.cust_Metric
            });
            arrAttributes.push({
                'value': oObjective.cust_PlannedAmout
            });
            arrAttributes.push({
                'label': 'Плановое значение',
                'label': 'Формула расчета',
                'value': oObjective.cust_Formula
            });
            node.icon = 'sap-icon://compare',
                node.attributes = arrAttributes;
        } else if (objectivePlanTypeCode === '1002') {
            arrAttributes.push({
                'label': 'Приоритет',
                'value': oObjective.cust_GoalCategoryNav ? oObjective.cust_GoalCategoryNav.label_localized : ''
            });
            arrAttributes.push({
                'label': 'Плановый результат',
                'value': oObjective.cust_ResultDescription
            });
            arrAttributes.push({
                'label': 'Срок выполнения',
                'value': oObjective.cust_ResultDate ? oObjective.cust_ResultDate.substr(0, 10) : ""
            });
            arrAttributes.push({
                'label': 'Методика',
                'value': oObjective.cust_ResultMethod
            });
            node.icon = 'sap-icon://target-group',
                node.attributes = arrAttributes;
        } else if (objectivePlanTypeCode === '1005') {
            arrAttributes.push({
                'label': 'Плановый результат',
                'value': oObjective.cust_ResultDescription
            });
            arrAttributes.push({
                'label': 'Срок выполнения',
                'value': oObjective.cust_ResultDate
            });
            arrAttributes.push({
                'label': 'Методика',
                'value': oObjective.cust_ResultMethod
            });
            node.icon = 'sap-icon://kpi-managing-my-area',
                node.attributes = arrAttributes;
        } else if (objectivePlanTypeCode === '1003') {
            arrAttributes.push({
                'label': 'Плановый результат',
                'value': oObjective.cust_ResultDescription
            });
            arrAttributes.push({
                'label': 'Срок выполнения',
                'value': oObjective.cust_ResultDate
            });
            arrAttributes.push({
                'label': 'Методика',
                'value': oObjective.cust_ResultMethod
            });
            arrAttributes.push({
                'label': 'Вес',
                'value': oObjective.cust_Weight
            });
            node.icon = 'sap-icon://two-keys',
                node.attributes = arrAttributes;
        } else {
            continue; // если иной план целей, то переход на следующий цикл
        }
        nodes.push(node);
    }
    var uniqueGroups = removeDuplicates(groups, 'key');

// console.log(groups.length)
//     for (var _i=0;_i<groups.length; _i++)
//     {
//         console.log(_i)
//         console.log(groups[_i])
//     }

    var groupsNodes =[]; // for searchi
//var arr=[]
    for (var _i=0; _i<nodes.length; _i++)
    {
        if (nodes[_i].group=="pos05")
        {
            groupsNodes.push(nodes[_i]);
            console.log(groupsNodes.length);
        }
    }

//    console.log(arr.length)

    // console.log(nodes.length);
    // for (var _i=0;_i<nodes.length;_i++){
    //     console.log(nodes[_i].group)
    // }
    var objGeneral = {
      //  nodes: nodes,
        lines: lines,
         grooups: groupsNodes

    };
    return objGeneral;


    console.log("dfdf")


};


// function processData(message) {
//     var filterYearValue = String(message.getProperty("filterYear"));
//     var filterPositionValue = String(message.getProperty("filterPosition"));
//     var rawdata = message.getBody(java.lang.String);
//     var objGeneral = processRawData(rawdata, filterYearValue, filterPositionValue);
//     message.setBody(JSON.stringify(objGeneral));
//     return message;
// }

module.exports = {
    processRawData: processRawData
};