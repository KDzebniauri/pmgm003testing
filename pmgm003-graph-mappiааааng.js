//importClass(com.sap.gateway.ip.core.customdev.util.Message);
Object.defineProperty(exports, "__esModule", {value: true});
var fs = require("fs");

function removeDuplicates(array, key) {
    var lookup = {};
    var result = [];
    for (var i = 0; i < array.length; i++) {
        if (!lookup[array[i][key]]) {
            lookup[array[i][key]] = true;
            result.push(array[i]);
        }
    }
    return result;
}

var processRawData = function processRawData(pRawData, pFilterYearValue, pFilterPositionValue) {
    /*KET*/// console.log("processRawData >> START");
    var jsonBody = JSON.parse(pRawData);
    if (!(jsonBody && jsonBody.cust_ObjectiveOnPositionDetails && jsonBody.cust_ObjectiveOnPositionDetails.cust_ObjectiveOnPositionDetails)) {
        throw new Error('Empty input content!');
    }

    function filterObjectives(objective) {
        return (
            objective.cust_ObjectiveOnPosition &&
            (!pFilterYearValue ||
                objective.cust_ObjectiveOnPosition.cust_ObjectivePlanNav.PickListValueV2.label_defaultValue &&
                objective.cust_ObjectiveOnPosition.cust_ObjectivePlanNav.PickListValueV2.label_defaultValue.substr(0, 4) === pFilterYearValue)
        );
    }

    function pushLines(fromParam, toParam) {
        lines.push({
            from: fromParam,
            to: toParam
        });
    }

    /*KET*/// console.log("processRawData >> STEP 1");
    var arrObjectives = jsonBody.cust_ObjectiveOnPositionDetails.cust_ObjectiveOnPositionDetails.filter(filterObjectives);
    var groups = []; // array with duplicate positions
    var nodes = [];
    var lines = [];

    for (var _i = 0; _i < arrObjectives.length; _i++) {
        var oObjective = arrObjectives[_i];
        var oHeader = oObjective.cust_ObjectiveOnPosition;
        var objectivePositionCode = oHeader.cust_ToPositions.Position.code;
        var matrixPositionCode = (oHeader.cust_ToPositions.Position.positionMatrixRelationship.PositionMatrixRelationship && oHeader.cust_ToPositions.Position.positionMatrixRelationship.PositionMatrixRelationship.relatedPositionNav.Position) ? oHeader.cust_ToPositions.Position.positionMatrixRelationship.PositionMatrixRelationship.relatedPositionNav.Position.code : null;
        var parentPositionCode = oHeader.cust_ToPositions.Position.parentPosition.Position ? oHeader.cust_ToPositions.Position.parentPosition.Position.code : null;

        var positionMap = {};
        var positionParentMap = {};

        positionMap[oObjective.externalCode] = objectivePositionCode;

        if (matrixPositionCode) {
            positionParentMap[oObjective.externalCode] = matrixPositionCode;
        } else {
            if (parentPositionCode) {
                positionParentMap[oObjective.externalCode] = parentPositionCode;
            }
        }
    }


    for (var _i = 0; _i < arrObjectives.length; _i++) {
        var oObjective = arrObjectives[_i];
        var oHeader = oObjective.cust_ObjectiveOnPosition;

        // add a group
        //  console.log(arrObjectives);
        groups.push({
            key: oHeader.cust_ToPositions.Position.code,
            title: oHeader.cust_ToPositions.Position.positionTitle
        });
        if (oObjective.cust_ParentObjective){
            if ( positionParentMap[oObjective.externalCode]== positionMap[oObjective.externalCode]) {
                pushLines(oObjective.cust_ParentObjective, oObjective.externalCode)
            } else if (positionMap[oObjective.externalCode] == positionParentMap[oObjective.externalCode]) {
                pushLines(oObjective.externalCode, oObjective.cust_ParentObjective)
            }

        }

        var objectivePlanTypeCode = oHeader.cust_ObjectivePlanTypeNav.PickListValueV2 ? oHeader.cust_ObjectivePlanTypeNav.PickListValueV2.externalCode : null;
        // prepare a node...
        var arrAttributes = [];
        // origin object for node
        var node = {
            key: oObjective.externalCode,
            title: oObjective.cust_GoalName,
            group: oHeader.cust_ToPositions.Position.code,
            shape: 'Box',
            status: objectivePlanTypeCode
        };
        if (objectivePlanTypeCode === '1001') {
            arrAttributes.push({
                'label': 'Единица измерения',
                'value': oObjective.cust_Metric
            });
            arrAttributes.push({
                'label': 'Плановое значение',
                'value': oObjective.cust_PlannedAmout
            });
            arrAttributes.push({
                'label': 'Формула расчета',
                'value': oObjective.cust_Formula
            });
            node.icon = 'sap-icon://compare',
                node.attributes = arrAttributes;
        } else if (objectivePlanTypeCode === '1002') {
            arrAttributes.push({
                'label': 'Приоритет',
                'value': oObjective.cust_GoalCategoryNav ? oObjective.cust_GoalCategoryNav.label_localized : ''
            });
            arrAttributes.push({
                'label': 'Плановый результат',
                'value': oObjective.cust_ResultDescription
            });
            arrAttributes.push({
                'label': 'Срок выполнения',
                'value': oObjective.cust_ResultDate
            });
            arrAttributes.push({
                'label': 'Методика',
                'value': oObjective.cust_ResultMethod
            });
            node.icon = 'sap-icon://target-group',
                node.attributes = arrAttributes;
        } else if (objectivePlanTypeCode === '1005') {
            arrAttributes.push({
                'label': 'Плановый результат',
                'value': oObjective.cust_ResultDescription
            });
            arrAttributes.push({
                'label': 'Срок выполнения',
                'value': oObjective.cust_ResultDate
            });
            arrAttributes.push({
                'label': 'Методика',
                'value': oObjective.cust_ResultMethod
            });
            node.icon = 'sap-icon://kpi-managing-my-area'ж
        } else if (objectivePlanTypeCode === '1003') {
            arrAttributes.push({
                'label': 'Плановый результат',
                'value': oObjective.cust_ResultDescription
            });
            arrAttributes.push({
                'label': 'Срок выполнения',
                'value': oObjective.cust_ResultDate
            });
            arrAttributes.push({
                'label': 'Методика',
                'value': oObjective.cust_ResultMethod
            });
            arrAttributes.push({
                'label': 'Вес',
                'value': oObjective.cust_Weight
            });
            node.icon = 'sap-icon://two-keys',
                node.attributes = arrAttributes;
        }
        nodes.push(node);
    }
    var uniqueGroups = removeDuplicates(groups, le'key');
    var objGeneral = {
        nodes: nodes,
        lines: lines,
        groups: uniqueGroups

    };
    return objGeneral;
};


function processData(message) {
    var filterYearValue = String(message.getProperty("filterYear"));
    var filterPositionValue = String(message.getProperty("filterPosition"));
    var rawdata = message.getBody(java.lang.String);
    var objGeneral = processRawData(rawdata, filterYearValue, filterPositionValue);
    message.setBody(JSON.stringify(objGeneral));
    return message;
}

module.exports = {
    processRawData: processRawData
};